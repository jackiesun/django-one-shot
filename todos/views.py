from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy

# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

    # def get_queryset(self):
    #     return TodoList.objects.all
class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create_list.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update_list.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete_list.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/create_item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/update_item.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
